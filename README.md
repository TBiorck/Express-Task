# Express-Task

This is the repository for the JavaScript course express task.

## Run with docker
Open CLI in the project root folder.

Create image with following command:
$docker build -t nameOfImage .

Get the image ID with:
$docker images

Run image:
$docker run -d -p 8080:8080 --name nameOfContainer idOfImage