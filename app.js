const express = require('express')
const path = require('path')

const app = express()
const { PORT = 8080 } = process.env

// Setup serving of static content
app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

// Endpoints
app.get('/', (req, res) => {
  return res.sendFile(path.join(__dirname, 'public', 'index.html'))
})

app.get('/data', (req, res) => {
  return res.sendFile(path.join(__dirname, 'data', 'data.json'))
})

app.get('/restaurants', (req, res) => {
  return res.sendFile(path.join(__dirname, 'public', 'restaurants.html'))
})

// Start server
app.listen(PORT, () => console.log(`Server is listening on port ${PORT}`))
