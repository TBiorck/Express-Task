FROM node:12

# Create the app directory
WORKDIR /usr/src/app

# Copy the package json files into container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the app
COPY . .

# Expose port
EXPOSE 8080

# Start the app within the container
CMD [ "node", "app.js" ]