const DATA_API = '/data'

async function getRestaurants () {
  const response = await fetch(DATA_API)
  const json = await response.json()

  addDataToHtml(json)
}

function addDataToHtml (restaurants) {
  const restaurantContainer = document.getElementById('restaurants')
  const template = document.getElementById('restaurant-template')

  restaurants.forEach(restaurant => {
    // Clone the template
    const clone = template.content.cloneNode(true)

    // Set title
    const title = clone.querySelector('h3')
    title.innerText = restaurant.name

    // Location, description and rating are the <p> elements.
    const data = clone.querySelectorAll('p')
    data[0].innerText = `Location: ${restaurant.location}`
    data[1].innerText = `Description: ${restaurant.description}`
    data[2].innerText = `Rating: ${restaurant.rating}`

    // Set the image
    const img = clone.querySelector('img')
    img.setAttribute('src', restaurant.img)

    // Add event on review button
    const reviewButton = clone.querySelector('button')
    addShowReviewEvent(reviewButton, restaurant.reviews)

    restaurantContainer.appendChild(clone)
  })
}

/**
 * Add a click eventlistener on a button. When the button is clicked
 * it sets the title and image value of the modal to the corresponding properties.
 * @param {HTMLElement} button
 */
function addShowReviewEvent (button, reviews) {
  const modalReviewContainer = document.getElementById('modal-review-container')

  button.addEventListener('click', event => {
    const reviewContainer = document.getElementById('modal-review-container')

    // If there are reviews in the modal, remove them before loading selected restaurant's reviews
    if (reviewContainer.getElementsByTagName('div').length !== 0) {
      clearModalReviews(reviewContainer)
    }

    const template = document.getElementById('review-template')

    // Add all reviews
    for (const review of reviews) {
      const clone = template.content.cloneNode(true)

      clone.querySelector('h5').innerText = review.author
      clone.querySelector('p').innerText = review.text

      modalReviewContainer.appendChild(clone)
    }
  })
}

/**
 * Reviews in the modal are each contained by a div. Remove all divs/reviews.
 */
async function clearModalReviews (reviewContainer) {
  const reviews = reviewContainer.getElementsByTagName('div')

  // The 'reviews' size is decreasing each iteration. Remove the first
  // item until all items has been removed
  while (reviews.length > 0) {
    reviews[0].remove()
  }
}
